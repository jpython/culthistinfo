# De quoi est fait un ordinateur ?

En gros, un ordinateur est un tas d'interrupteurs 
commutable (on/off) reliés entre eux.

Technique : mécanique, électro-magnétique (relais),
électrique (tubes à vide), électronique (transistors),
aquatique (si si)

Exemple :

- avec de l'eau : https://www.youtube.com/watch?v=IxXaizglscw&ab_channel=SteveMould
- relais : https://www.youtube.com/watch?v=F__TroRudww&ab_channel=ClicketyClack
- que fait l'UAL (Unité arithmétique et logique : un processeur) : https://www.youtube.com/watch?v=Z5JC9Ve1sfI&ab_channel=TomScott
- un simulateur d'ordinateur : https://www.youtube.com/watch?v=QZwneRb-zqA&ab_channel=SebastianLague
- La page de Ben Eater : https://eater.net
  - Série sur les transistor (physique/chimie)
  - Construire les composants de base : registres, mémoire, bus,
    cpu
  - À partir d'un CPU existant (6502, comme l'Apple II, le 
    Commodore 64, etc.), construire un ordinateur
  - Carte vidéo, communication série, etc.

- Comment sont produits les semi-conducteurs modernes, l'histoire
 de la société ASMR : https://www.youtube.com/watch?v=zQu_TMgHO98&ab_channel=vprodocumentary
