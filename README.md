# Histoire et culture de l'informatique

03/01/2021 - v0.99a

mise à jour mineure : Mar mar 9 18:59:33 CET 2021

## PDF

Contenu PDF à jour : https://framagit.org/jpython/culthistinfo/-/raw/master/Docs/Teach/cultHistInfo.pdf?inline=false

Proposition de recherches personnelles : https://framagit.org/jpython/culthistinfo/-/tree/master/Docs/Atelier

## Plan du cours
- Calcul et algorithmes
- Systèmes de numérations
- Calculatrices mécaniques
- Machine de Babbage et métiers à tisser
- Théories mathématiques du calcul effectif
- Électronique
- Systèmes d'exploitation
- UNIX, GNU et Linux
- Microsoft : MS-DOS et MS Windows
- Internet
- Le Cloud
- Langages de programmations
- Bases de données
- Cryptographie
