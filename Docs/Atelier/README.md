# Histoire et Culture de l'Informatique : Études de cas

Il n'est demandé que de traiter **deux parties** parmi
les trois détaillées ci-dessous, _à votre convenance_.

Chaque partie est à rédiger en une page au maximum sur
la base d'une recherche de documentation sur l'Internet
qu'il vous revient de synthétiser.

Le format du document à fournir peut être PDF, Markdown,
texte simple ou encore ODT.

## Un autre système aux laboratoires Bell...

Une large partie des créateurs d'UNIX aux Bell Labs
ont créé un autre système d'exploitaion, 
en recherche et développement,
après qu'UNIX ne soit devenu un système industriel
et commercial.

Ce système s'appelle _Plan 9_, il a une mascotte encore
plus migonne qu'un manchot :

<img alt="Glenda" src="glenda.jpg"  width="50%">

Décrivez succintement la motivation pour créer un nouveau
système plutôt que de continuer à partir d'UNIX.

Qu'est-ce que ce système a en commun avec UNIX ? En quoi
diffère-t-il d'UNIX ?

Est-il encore développé aux Bell Labs ? Possède-t-il
des utilisateurs actuellement ? Y a-t-il des versions
dérivées développées par des tiers qui sont disponibles ?

Peut-on trouver des technologies développées initialement
pour  _Plan 9_ qui se retrouvent 
ailleurs ? Lesquelles ?

Pourquoi n'a-t-il pas connu de succès, même après avoir
été publié selon une licence libre/open source, selon
vous ?

## UNIX (ou pas...) contre Linux

Au début du XXIème siècle la société SCO a intenté des
actions en justice et menacé légalement des entreprises
éditrices, contributrices ou utilisatrices de GNU/Linux.

Quels étaient les accusations portées par SCO ? Envers
qui ?

Quelle était la base légale sur laquelle SCO se 
basait pour se prétendre lésé ?

Résumez l'issue des différentes procédures
judiciaires ? Qu'en pensez-vous ?

## GIT

Quel système de gestion de version Linus Torvalds a-t-il utilisé pour gérer les sources du noyau Linux avant qu'il
ne crée lui-même git ?

Ce système de gestion de version était-il un logiciel
libre ?

Quels sont les caractéristiques principales de git qui le distinguent
des autres systèmes courants de gestion de version ?
 
