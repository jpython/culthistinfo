# Les unités de mesures en informatique

Unité d'information la plus petite : bit (0 ou 1)
(Binary digIT)

octet : 8 bits

traduit habituellement en anglais par byte
(en fait c'est faux et c'est vrai)
(octet existe en anglais et la taille d'un
 byte n'est pas nécessairement de 8 bits,
 mais c'est très rare)

on parle souvent aussi de "mots" ("words") qui
fait généralement deux octets (mais pas toujours)

des kilos, mégas, gigas, tera, peta, hexa, zeta, iota...

Un kilo octet c'est combien d'octets ?

- spontanément les informaticiens on commencé à
  considérer que 1ktruc = 1024truc (`1024 = 2**10̀`)
- comité international ISO fixe les normes en matière
  d'unité, ils ont refusé de garder l'usage courant
  en informatique :

  - "ISO" : 1Ko = 1000o, 1Mo = 1000Ko, etc.
      kilo, mega, etc.
  - "non ISA" :  1Kio = 1024o, 1Mio 1024Kio, ...
      kibi, mibi, etc.

Vitesse de transmission (ports série, hdmi, réseau, etc.)

bit/s : baud

Si vous avez une connexion internet, la FAI va vous fournir
un débit en Mb/s ou Gb/s, et vous constaterez en téléchargeant
un fichier que vous aurez sera souvent en gros la moitié
de cette valeur. "Overhead" (surcharge) due aux protocoles
IP/TCP/HTTP....


