# Installer Debian GNU/Linux dans VirtualBox 

## Installer VirtualBox

Si vous utilisez macOS ou MS Windows vous pouvez
télécharger VirualBox là : https://www.virtualbox.org/

(Sous GNU/Linux il est disponible à partir des dépots DEB ou RPM de votre distribution préférée)

## Télécharger l'image iso de Debian

Télécharger l'image ISO d'installation de Debian GNU/Linux minimal
(dite _netinst_) :

https://www.debian.org/CD/netinst/index.fr.html

Préférez la version dite _stable_ (mais vous pouvez expérimenter
avec _testing_ les fonctionnalité de la prochaine version) pour
amd64 (processeur de la famille AMD/Intel 64 bits) :

https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.6.0-amd64-netinst.iso

(ce fichier peut aussi se graver sur un CD-ROM ou DVD ou être 
copié sur une clef usb avec un outil spécifique sous MS Windows
(Rufus : https://rufus.ie/fr/) ou bien la commande `dd` sous
GNU/Linux, macOS, et autres UNIX)

*Note* : On peut installer Debian "à côté" de MS Windows sur
une machine physique, si vous le faites sauvegardez vos précieuses
données, une fausse manipulation pourrait supprimer MS
Windows.

## Démarrer l'installation avec VirtualBox

Lancer VirtualBox :
- Cliquez sur le bouton "Nouvelle"
- Nom : Debian 1HIST
- ISO Image / Autres... / Téléchargement : sélection le
  fichier debian-11.6.0-adm64-netinst.iso
  Cochez : Skip Unattended Installation
- Cliquez sur Suivant
  - Mémoire : je conseille 4096
  - Cochez : Enable EFI
  - Suivant
- Taille du disque dur : 20Gio
  - Suivant
- Finish

- Cliquez sur configuration
  - Système / Processeur / changez de 1 à 2
  - Réseau : Mode d'accès ; changez en "Accès par pont"

Valider la conf et Démarrer la VM,

Choisir Graphical Install ou Install puis Entrée

- Sélectionner French - Français
- Sélectionner le clavier français (ou autre si nécessaire)

- Choix du nom de la machine ; suggestion : un nom de ville
- Domaine : laissez vide
- Mot de passe de l'utilisateur, pour l'instant mettez rootpw

- Comme nom complet mettez seulement votre prénom, avec une
  capitale
- l'identifant sera donc votre prénom en minuscule 
- le mot de passe, pour l'instant mettez userpw

- Partionnement des disques : assisté - utiliser un disque entier
- Sélectionner l'unique disque proposé
- Tout dans une seule partition
- Valider et selectionner oui
- Faut-il analyser d'autres support d'installation : non
- Laissez France, deb.debian.org, mandataire : vide
- Populatity contest : laissez non
- Selection des logiciels : 
   - Environnemnt de bureau Debian
   - Gnome
   - Serveur SSH
   - Utilitaires usuels du système
- Continuer

- Si la question est posée concernant l'installation de grub : /dev/sda

# Finalisation de l'installation 

- Au redémarrage vous pouvez vous connecter en selectionnant 
 votre prénom et en saisissant votre mot de passe
- Cliquer sur "Activité"
- Tapez dans le champs texte : term
- Un icône de Terminal apparaît, cliquez dessus
- Cliquez à nouveau sur activité
- L'icône de Terminal est dans la barre à gauche
- Clic droit sur cette icône 
- Sélectionner "Ajoutez au favori"

Ainsi le terminal de Gnome sera toujours facilement accessible
en cliquant sur activité, il sera toujours là (dans le "dock")
même si vous le fermez... 

Saluez cette application : c'est celle avec laquelle vous serez,
et de loin, le plus souvent en interaction dans votre vie
professionnelle.

N'hésitez pas à explorer les possibilité de paramètrage : taille
de texte, couleurs, etc. (cf. Préférences, paramètre, profils)

## Installer les add-ons invités

1. Dans le terminal : on devient "root" (administrateur) en
   saisissant le mot de passe spécifié à l'installation
   (rootpw)
   _ le mot de passe ne s'affiche pas, c'est normal_
2. On va faire un truc pratique tout de suite : s'ajouter
   au groupe d'utilisateur "sudo", ce qui nous permettra
   par la suite de passer en root (administrateur) avec
   la commande `sudo` (qui vous demandera _votre_ mot de
   passe, normalement "userpw") :

~~~~Bash
$ su -
Mot de passe:
# usermod -a -G sudo votre_identifiant
~~~~

(notez que la fin de l'invite est `$` quand vous êtes un utilisateur
"normal" et `#` quand vous êtes "root".

On installe les pilotes Linux pour Virtual Box :

Dans le terminal :

~~~~Bash
# apt update
# apt -y install linux-headers-$(uname -r) dkms 
~~~~

Ceci fait : dans le menu de VirtualBox : Périphérique,
sélectionner "Insérer l'image CD des Additions invité"

Ensuite dans le Terminal :
~~~~Bash
# mount /media/cdrom
# bash /media/cdrom/VBoxLinuxAdditions.run
~~~~

(si une fenêtre demande à exécuter un programme dite "Annuler")

Attendre, quand l'invite du Shell revient dans le Terminal :

~~~~Bash
# reboot
~~~~

Au redémarrage, connectez-vous au bureau.

Diminuez/Augmenter (plain écran) la fenêtre de la machine
virtuelle vous constaterez, avec joie, que le bureau occupe
automatiquement tout l'espace possible.

Dans le menu de Virtual Box, selectionnez Périphériques
et Presse-papier partagé - Bidirectionnel

Il existe plusieurs logiciel du type Terminal, l'un d'entre
eux imite les terminaux sur écran cathodique de l'ancien
temps :

~~~~Bash 
$ sudo apt -y install cool-retro-term
$ cool-retro-term &
~~~~

(et plein d'autres logiciels sympas sont disponibles tout aussi
facilement) 

Bienvenue sous GNU/Linux !!!
